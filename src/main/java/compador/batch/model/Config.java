package compador.batch.model;

import java.io.Serializable;

public class Config implements Serializable {
	private Integer id;
	private String nombre;
	
	public Config() {
		
	}
	
	public Config(Integer id, String nombre) {
		this.id=id;
		this.nombre=nombre;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
    @Override
    public String toString() {
        return "id: " + id + ", nombre: " + nombre;
    }
}
