package compador.batch.chunck.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;

import compador.batch.model.Config;
import compador.batch.model.Persona;

public class PersonaItemProcessor implements ItemProcessor<Persona, Persona> {

    private static final Logger log = LoggerFactory.getLogger(PersonaItemProcessor.class);
    private static Config conf=null;

    
    @AfterStep
	public void getConfiguration(StepExecution stepExecution) {
		//this.stepExecution = stepExecution;
    	JobExecution jobExecution = stepExecution.getJobExecution();
    	//arg1.getStepContext().getStepExecution().getJobExecution(); 	
		ExecutionContext jobContext = jobExecution.getExecutionContext();
    	conf=(Config) jobContext.get("configJob");
    	log.info("[PersonaItemProcessor] - getConfiguration - (Config : "+conf.getId()+")");
    	
	}
    
    @Override
    public Persona process(final Persona persona) throws Exception {
    	//log.info("[PersonaItemProcessor] - process: (Conf id:"+conf.getId());
    	final String nombre = persona.getNombre().toUpperCase();
        final String apellido = persona.getApellido().toUpperCase();
        final Persona correccionPersona = new Persona(nombre, apellido);

        log.info("Corrigiendo (" + persona + ") a (" + correccionPersona +")");

        return correccionPersona;
    }

}
