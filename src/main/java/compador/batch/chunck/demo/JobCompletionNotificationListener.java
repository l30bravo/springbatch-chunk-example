package compador.batch.chunck.demo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import compador.batch.model.Persona;

@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

	private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public JobCompletionNotificationListener(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("Job Terminado - verificando resultados");

			List<Persona> results = jdbcTemplate.query("SELECT nombre, apellido FROM persona", new RowMapper<Persona>() {
				@Override
				public Persona mapRow(ResultSet rs, int row) throws SQLException {
					return new Persona(rs.getString(1), rs.getString(2));
				}
			});

			for (Persona person : results) {
				log.info("Encontrado <" + person + "> en la base de datos.");
			}
		}
	}
}
