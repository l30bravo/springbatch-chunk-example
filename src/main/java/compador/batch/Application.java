package compador.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"jobs/job-config.xml","config/context.xml", "config/database.xml"})
public class Application {
		
	 public static void main(String[] args) throws Exception {
	        SpringApplication.run(Application.class, args);
	 }
		
}
