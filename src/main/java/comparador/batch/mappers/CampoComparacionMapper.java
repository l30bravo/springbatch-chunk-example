package comparador.batch.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comparador.batch.model.CampoComparacion;
import comparador.batch.model.Origen;

public class CampoComparacionMapper {
private static final Logger log = LoggerFactory.getLogger(ConfigMapper.class);
	
	public static List<CampoComparacion> mapObjet(ResultSet rs) {
		List<CampoComparacion> campos = new ArrayList<CampoComparacion>();
		
		try {
			while( rs.next() ) {
				CampoComparacion campo = new CampoComparacion();
				campo.setId(rs.getLong("ID_CAMP"));
				campo.setIdOrigen(rs.getLong("ID_ORI"));
				campo.setTipoLlave(rs.getString("TIPO_LLAVE"));
				campo.setCampo(rs.getString("CAMPO"));
				campo.setReferencia(rs.getString("REFERENCIA"));
				campo.setCampoOrden(rs.getInt("CAMPO_ORDEN"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return campos;
		
	}
}
