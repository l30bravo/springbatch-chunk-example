package comparador.batch.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import comparador.batch.model.Persona;

//public class PersonaRowMapper implements RowMapper<Persona>{
public class PersonaMapper implements RowMapper<Persona>{
	
	@Override
    public Persona mapRow(ResultSet rs, int rowNum) throws SQLException {
        Persona persona = new Persona();
        persona.setNombre((rs.getString("nombre")));
        persona.setApellido((rs.getString("apellido")));
        return persona;
    }
}
