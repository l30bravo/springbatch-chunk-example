package comparador.batch.mappers;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comparador.batch.model.CampoComparacion;
import comparador.batch.model.Config;
import comparador.batch.model.Origen;
import comparador.batch.utils.MyDatabaseUtil;
import comparador.batch.utils.PropertiesLoader;

public class OrigenMapper {
private static final Logger log = LoggerFactory.getLogger(ConfigMapper.class);
	
	public static List<Origen> mapObjet(ResultSet rs) {
		List<Origen> origenes = new ArrayList<Origen>();
		log.info("[OrigenMapper] Mapeando Origenes");
		try {
				while( rs.next() ) {
					Long idConf = rs.getLong("ID_CONF");
					Origen origen = new Origen();
					origen.setId(rs.getLong("ID_ORI"));
					origen.setIdConf(idConf);
					origen.setOrigenTipo(rs.getString("ORIGEN_TIPO"));
					origen.setTipoConexion(rs.getString("TIPO_CONEXION"));
					origen.setUsuario(rs.getString("USUARIO"));
					origen.setClave(rs.getString("CLAVE"));
					origen.setQuery(rs.getString("QUERY"));
					origen.setArchivoPath(rs.getString("ARCHIVO_PATH"));
					origen.setArchivoNombre(rs.getString("ARCHIVO_NOMBRE"));
					origen.setArchivoTipo(rs.getString("ARCHIVO_TIPO"));
					origen.setArchivoSeparador(rs.getString("ARCHIVO_SEPARADOR"));
					origen.setCamposComparacion(addCamposComparacion(idConf));
					
					origenes.add(origen);
			}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.info("[ConfigMapper] ERROR: "+e.toString());
			}

		return origenes;
	}
	
	
	public static List<CampoComparacion> addCamposComparacion(Long idConf){
		List<CampoComparacion> campos = new ArrayList<CampoComparacion>();
		Properties properties;
		try {
			//Carga la Query de Origenes
			properties = PropertiesLoader.loadProperties();
			String query=properties.getProperty("sql.camposcomparacion").replace("$IDCONF", idConf.toString());
			log.debug("[OrigenMapper] addOrigenes Query: "+query);
			//Mapea el objeto
			campos = CampoComparacionMapper.mapObjet( MyDatabaseUtil.executeQuery(query) );
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.info("[OrigenMapper] addOrigenes  - ERROR :"+e.toString());
			e.printStackTrace();
		}
		
		return campos;
	}
}
