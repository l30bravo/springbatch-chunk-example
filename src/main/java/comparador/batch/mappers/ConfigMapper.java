package comparador.batch.mappers;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import comparador.batch.model.Config;
import comparador.batch.model.Origen;
import comparador.batch.utils.MyDatabaseUtil;
import comparador.batch.utils.PropertiesLoader;

public class ConfigMapper {

	private static final Logger log = LoggerFactory.getLogger(ConfigMapper.class);
	
	public static Config mapObjet(ResultSet rs) {

		Config config = new Config();
		try {
			rs.next();
			try {
				Integer idConf=rs.getInt("ID_CONF");
				log.info("[ConfigMapper] Mapeando Config");
				config.setId( idConf );
				config.setIdAgrupador( rs.getInt("ID_AGRUPADOR") );
				config.setDescrip( rs.getString("DESCRIPCION") );
				config.setOrigenes(addOrigenes(idConf));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.info("[ConfigMapper] ERROR: "+e.toString());
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			log.info("ERROR: "+e1.toString());
			e1.printStackTrace();
		}
		return config;
	}
	
	public static List<Origen> addOrigenes(Integer idConf){
		List<Origen> origenes = new ArrayList<Origen>();
		Properties properties;
		try {
			//Carga la Query de Origenes
			properties = PropertiesLoader.loadProperties();
			String query=properties.getProperty("sql.origen").replace("$IDCONF", idConf.toString());
			log.debug("[ConfigMapper] addOrigenes Query: "+query);
			//Mapea el objeto
			origenes = OrigenMapper.mapObjet( MyDatabaseUtil.executeQuery(query) );
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.info("[ConfigMapper] addOrigenes  - ERROR :"+e.toString());
			e.printStackTrace();
		}
		return origenes;

	}
}
