package comparador.batch.model;

import java.io.Serializable;

public class Accion implements Serializable{
	private Long id;
	private Long idConf;
	private Long idEstadoComparacion;
	//EstadoComparacion estado comparacion.
	private Long idTipoAccion;
	private String flujo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdConf() {
		return idConf;
	}
	public void setIdConf(Long idConf) {
		this.idConf = idConf;
	}
	public Long getIdEstadoComparacion() {
		return idEstadoComparacion;
	}
	public void setIdEstadoComparacion(Long idEstadoComparacion) {
		this.idEstadoComparacion = idEstadoComparacion;
	}
	public Long getIdTipoAccion() {
		return idTipoAccion;
	}
	public void setIdTipoAccion(Long idTipoAccion) {
		this.idTipoAccion = idTipoAccion;
	}
	public String getFlujo() {
		return flujo;
	}
	public void setFlujo(String flujo) {
		this.flujo = flujo;
	}
}
