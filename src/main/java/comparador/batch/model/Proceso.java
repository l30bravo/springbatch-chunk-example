package comparador.batch.model;

import java.io.Serializable;

public class Proceso implements Serializable {
	
	private Long id;
	private Integer id_conf=-1;
	private String idEstado="N.A";
	private String description;
	private String date_ini;
	private String date_end;
	private Long id_process_partner;
	

	public Proceso() {	

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public String getIdEstado() {
		return idEstado;
	}


	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public Integer getId_conf() {
		return id_conf;
	}

	public void setId_conf(Integer id_conf) {
		this.id_conf = id_conf;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate_ini() {
		return date_ini;
	}

	public void setDate_ini(String date_ini) {
		this.date_ini = date_ini;
	}

	public String getDate_end() {
		return date_end;
	}

	public void setDate_end(String date_end) {
		this.date_end = date_end;
	}

	public Long getId_process_partner() {
		return id_process_partner;
	}

	public void setId_process_partner(Long id_process_partner) {
		this.id_process_partner = id_process_partner;
	}
}
