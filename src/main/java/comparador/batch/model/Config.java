package comparador.batch.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Config implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2577186743176296046L;
	private Integer id=-1;
	private Integer idAgrupador=-1;
	private String descrip="N.N";
	private List<Origen> origenes;
	
	public Config() {
		this.setOrigenes(new ArrayList<Origen>());
	}
	
	public Config(Integer id, String descrip) {
		this.id=id;
		this.descrip=descrip;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescrip() {
		return descrip;
	}
	public void setDescrip(String nombre) {
		this.descrip = nombre;
	}

	public Origen getOrigenMandatorio() {
		for (Origen origen : this.origenes) {
			if(origen.getOrigenTipo().equals("M")) {
				return origen;
			}
		}
		return null;
	}

	/*
	public void setOrigenMandatorio(Origen origenMandatorio) {
		this.origenMandatorio = origenMandatorio;
	}*/

	public Origen getOrigenEsclavo() {
		for (Origen origen : this.origenes) {
			if(origen.getOrigenTipo().equals("E")) {
				return origen;
			}
		}
		return null;
	}

	/*
	public void setOrigenEsclavo(Origen origenEsclavo) {
		this.origenEsclavo = origenEsclavo;
	}*/

	public Integer getIdAgrupador() {
		return idAgrupador;
	}

	public void setIdAgrupador(Integer idAgrupador) {
		this.idAgrupador = idAgrupador;
	}

	public List<Origen> getOrigenes() {
		return origenes;
	}

	public void setOrigenes(List<Origen> origenes) {
		this.origenes = origenes;
	}

}

