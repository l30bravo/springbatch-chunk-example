package comparador.batch.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Origen implements Serializable{
	private Long id;
	private Long idConf;
	private String origenTipo;//Mandatorio o Esclavo
	private String tipoConexion; //db o archivo
	private String usuario;
	private String clave;
	private String query;
	private String archivoPath;
	private String archivoNombre;
	private String archivoTipo;
	private String archivoSeparador;
	private List<CampoComparacion> camposComparacion;
	
	public Origen() {
		this.setCamposComparacion(new ArrayList<CampoComparacion>());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdConf() {
		return idConf;
	}
	public void setIdConf(Long idConf) {
		this.idConf = idConf;
	}
	public String getOrigenTipo() {
		return origenTipo;
	}
	public void setOrigenTipo(String origenTipo) {
		this.origenTipo = origenTipo;
	}
	public String getTipoConexion() {
		return tipoConexion;
	}
	public void setTipoConexion(String tipoConexion) {
		this.tipoConexion = tipoConexion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getArchivoPath() {
		return archivoPath;
	}
	public void setArchivoPath(String archivoPath) {
		this.archivoPath = archivoPath;
	}
	public String getArchivoNombre() {
		return archivoNombre;
	}
	public void setArchivoNombre(String archivoNombre) {
		this.archivoNombre = archivoNombre;
	}
	public String getArchivoTipo() {
		return archivoTipo;
	}
	public void setArchivoTipo(String archivoTipo) {
		this.archivoTipo = archivoTipo;
	}
	public String getArchivoSeparador() {
		return archivoSeparador;
	}
	public void setArchivoSeparador(String archivoSeparador) {
		this.archivoSeparador = archivoSeparador;
	}

	public List<CampoComparacion> getCamposComparacion() {
		return camposComparacion;
	}

	public void setCamposComparacion(List<CampoComparacion> camposComparacion) {
		this.camposComparacion = camposComparacion;
	}
	
	/*
	public CampoComparacion getCampoComparacion() {
		return campoComparacion;
	}
	public void setCampoComparacion(CampoComparacion campoComparacion) {
		this.campoComparacion = campoComparacion;
	}*/
	
}
