package comparador.batch.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CampoComparacion  implements Serializable{
	private Long id;
	private Long idOrigen;
	private String tipoLlave; // PKS O SKS
	private String campo; // nombre de la columna
	private String referencia; // nombre columna o posicion 
	private Integer campoOrden;
	
	//private List<CampoComparacion> campos = new ArrayList<CampoComparacion>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdOrigen() {
		return idOrigen;
	}
	public void setIdOrigen(Long idOrigen) {
		this.idOrigen = idOrigen;
	}
	public String getTipoLlave() {
		return tipoLlave;
	}
	public void setTipoLlave(String tipoLlave) {
		this.tipoLlave = tipoLlave;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public Integer getCampoOrden() {
		return campoOrden;
	}
	public void setCampoOrden(Integer campoOrden) {
		this.campoOrden = campoOrden;
	}
}
