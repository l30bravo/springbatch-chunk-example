package comparador.batch.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesLoader {
	
	private static final Logger log = LoggerFactory.getLogger(PropertiesLoader.class);
	private static Properties prop;
	
	private static String file="comparador.properties"; 
	
	
	public static Properties loadProperties() throws IOException {
		prop = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();    
		InputStream stream = loader.getResourceAsStream(file);
		if (stream != null) {
			prop.load(stream);
		} else {
			log.info("property file '" + file + "' not found in the classpath");
			throw new FileNotFoundException("property file '" + file + "' not found in the classpath");
		}
		
		return prop;
	}
	
	/*
	public Properties getProperties() {
		return this.prop;
	}*/
}
