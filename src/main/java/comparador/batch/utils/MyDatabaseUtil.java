package comparador.batch.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MyDatabaseUtil {
	
	private static final Logger log = LoggerFactory.getLogger(MyDatabaseUtil.class);
	
    private static Connection cn;
    private static Statement st;
    private static ResultSet rs;
	
    public static Connection getConnection() throws IOException {
    	Properties properties = PropertiesLoader.loadProperties();
    	
        try {
            //return DriverManager.getConnection("jdbc:derby:./target/mydb;create=true");
        	log.info("[MyDatabaseUtil] - Conectando con la DB ...");
        	String usuario=properties.getProperty("db.oracle.user");
        	String clave=properties.getProperty("db.oracle.passwd");
        	String host=properties.getProperty("db.oracle.host");
        	String port=properties.getProperty("db.oracle.port");
        	String sid=properties.getProperty("db.oracle.sid");
        	return DriverManager.getConnection("jdbc:oracle:thin:@"+host+":"+port+":"+sid, usuario, clave);
        } catch (SQLException e) {
        	log.error("Error: "+e.toString());
            throw new RuntimeException(e);
        }
    }
    
    
    public static ResultSet executeQuery(String query) {

        try {
        	log.info("[MyDatabaseUtil] - Ejecutando :"+query);
            cn = getConnection();
            st = cn.createStatement();
			rs = st.executeQuery(query);
			return rs;

			
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			log.info(e.toString());
			//e.printStackTrace();
		}
        
        return null;
    }
    /*
    private String cleanQuery(String query) {
    	
    }*/
        
}
