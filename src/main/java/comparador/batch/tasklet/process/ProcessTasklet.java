package comparador.batch.tasklet.process;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;

import comparador.batch.model.Proceso;

public class ProcessTasklet implements Tasklet, InitializingBean{
	
	private static final Logger log = LoggerFactory.getLogger(ProcessTasklet.class);
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		log.info("[ProcessTasklet] - Run afterPropertiesSet");
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		log.info("[ProcessTasklet] - Creando Proceso");
		// TODO Auto-generated method stub
		JobExecution jobExecution = arg1.getStepContext().getStepExecution().getJobExecution();
		ExecutionContext jobContext = jobExecution.getExecutionContext();
		log.info("[ProcessTasklet] - Cargando proceso al Context del Job");
		//jobContext.put("procesoJob", ProcesoController.loadProcess(jobExecution));
		jobContext.put("procesoJob", loadProcess(jobExecution));
		return null;
	}
	
	
	private Proceso loadProcess(JobExecution jobExecution) {
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		
		log.info("[ProcessTasklet] loadProcess");
		Long idProcess=System.currentTimeMillis();
		
		Proceso process = new Proceso();
		process.setId( idProcess );
		process.setIdEstado( jobExecution.getStatus().toString() );
		process.setDescription( "Comparacion  "+idProcess );
		process.setDate_ini( ft.format( jobExecution.getStartTime()) );
		process.setDate_end( " " );
		process.setId_process_partner((long) -1);
		
		return process;
	}

}
