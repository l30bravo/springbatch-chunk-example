package comparador.batch.tasklet.config;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;

import comparador.batch.mappers.ConfigMapper;
import comparador.batch.model.Config;
import comparador.batch.model.Proceso;
import comparador.batch.utils.MyDatabaseUtil;
import comparador.batch.utils.PropertiesLoader;

public class ConfigureTasklet implements Tasklet, InitializingBean{
	
	//private Config confTest=null;
	
	private static final Logger log = LoggerFactory.getLogger(ConfigureTasklet.class);
	
	
	//PASAR A CONFIG CONTROLLER????
    public Config loadConfig(Integer idConf) throws IOException {
    	Properties properties = PropertiesLoader.loadProperties(); //carga la conf del comparador
    	String query=properties.getProperty("sql.configuracion").replace("$IDCONF", idConf.toString()); // carga la query para extraer la conf
    	log.debug("[ConfigureTasklet] loadConfig Query: "+query);
    	Config conf = new Config();
    	conf = ConfigMapper.mapObjet( MyDatabaseUtil.executeQuery(query) ); //mapea la conf de la query y la guarda en el objeto
    	
    	log.info("[ConfigureTasklet] - Cargando Configuración");
    	return conf;
    }

    //Antes de configurar
    @Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		log.debug("[ConfigureTasklet] - Run afterPropertiesSet");
	}

    //Ejecución del step tasklet
	@Override
	public RepeatStatus	 execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		Integer idConf=1; //TOMARLA COMO PARAMETRO DE ENTRADA EN LA EXEC
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		// TODO Auto-generated method stub
		
		log.info("[ConfigureTasklet] - Creando la configuracion");
		JobExecution jobExecution = arg1.getStepContext().getStepExecution().getJobExecution(); 	
		ExecutionContext jobContext = jobExecution.getExecutionContext();

		//Actualizando proceso.
		Proceso process = (Proceso) jobContext.get("procesoJob");
		process.setIdEstado("CONFIGURANDO");
		process.setDate_end( ft.format(jobExecution.getCreateTime()) );
		jobContext.put("procesoJob", process);
		
		//Cargando config
		jobContext.put("configJob", loadConfig(idConf));
		return null;
	}
	

	
}
