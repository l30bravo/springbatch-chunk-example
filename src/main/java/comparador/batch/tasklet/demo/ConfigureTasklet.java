package comparador.batch.tasklet.demo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;

import compador.batch.model.Config;

public class ConfigureTasklet implements Tasklet, InitializingBean{
	
	private Config confTest=null;
	
	private static final Logger log = LoggerFactory.getLogger(ConfigureTasklet.class);
	
    public Config loadConfig(int idConf) {
    	Config conf = new Config();
    	conf.setId(idConf);
    	conf.setNombre("ConfigDemo");
    	log.info("[ConfigureTasklet] - Cargando Configuración");
    	return conf;
    }

    //Antes de configurar
    @Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		log.info("[ConfigureTasklet] - Run afterPropertiesSet");
	}

    //Ejecución del step tasklet
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		// TODO Auto-generated method stub
		log.info("[ConfigureTasklet] - Run execute");
		JobExecution jobExecution = arg1.getStepContext().getStepExecution().getJobExecution(); 	
		ExecutionContext jobContext = jobExecution.getExecutionContext();
		jobContext.put("configJob", loadConfig(1));
		this.confTest = (Config) jobContext.get("configJob");
		log.info("[ConfigureTasklet] - "+jobContext.get("configJob").toString());
		log.debug("[ConfigureTasklet] - Config [ ID:"+this.confTest.getId()+", NOMBRE:"+this.confTest.getNombre()+" ]");

		return null;
	}
	
}
