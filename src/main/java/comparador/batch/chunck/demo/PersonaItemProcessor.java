package comparador.batch.chunck.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;

import comparador.batch.model.Config;
import comparador.batch.model.Persona;
import comparador.batch.model.Proceso;

public class PersonaItemProcessor implements ItemProcessor<Persona, Persona> {

    private static final Logger log = LoggerFactory.getLogger(PersonaItemProcessor.class);
    private static Config conf=null;

    
    //@AfterStep
    @BeforeStep
	public void getConfiguration(StepExecution stepExecution) {
		//this.stepExecution = stepExecution;
    	JobExecution jobExecution = stepExecution.getJobExecution();
    	//arg1.getStepContext().getStepExecution().getJobExecution(); 	
		ExecutionContext jobContext = jobExecution.getExecutionContext();
		
		//Extraer datos de la Config
    	conf=(Config) jobContext.get("configJob");
    	log.info("[PersonaItemProcessor] Extrae Config {Config : "+conf.getId()+", Origen-tipo: "+conf.getOrigenEsclavo().getOrigenTipo()+"}");

    	//Estraer datos del proceso
    	Proceso process = (Proceso)jobContext.get("procesoJob");
    	log.info("[PersonaItemProcessor] Extrae Proceso {id:"+process.getId()+", descrip:"+process.getDescription()+", status:"+process.getIdEstado()+"}");
    	
	}
    
    @Override
    public Persona process(final Persona persona) throws Exception {
    	//log.info("[PersonaItemProcessor] - process: (Conf id:"+conf.getId());
        log.info("[PersonaItemProcessor] config.toString: {"+conf.toString()+"}");
    	final String nombre = persona.getNombre().toUpperCase();
        final String apellido = persona.getApellido().toUpperCase();
        final Persona correccionPersona = new Persona(nombre, apellido);
        log.info("Corrigiendo (" + persona + ") a (" + correccionPersona +")");


        return correccionPersona;
    }

}
