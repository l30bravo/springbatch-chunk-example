DROP TABLE persona IF EXISTS;

CREATE TABLE persona  (
    persona_id BIGINT IDENTITY NOT NULL PRIMARY KEY,
    nombre VARCHAR(20),
    apellido VARCHAR(20)
);
