# COMPARADOR

Proyecto que realiza proceso de comparación de dos orígenes de datos y genera un resultado con los items comparados, su estado y las acciones a realizar en los esquemas o archivos correspondientes.

# PACKAGE

```bash
mvn package
```

# DEPLOY

```bash
scp ${ls -l target/SpringBatchDemo*.jar | awk {'print $9'}} $USER@$HOST:$PATH
```

* $USER	= usuario unix
* $HOST = IP de la maquina
* $PATH = ruta del directorio de la maquina


# EJECUCIÓN

```bash
java -jar SpringBatchDemo*.jar 
```
